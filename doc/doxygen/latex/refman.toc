\contentsline {chapter}{\numberline {1}Qt\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Stego\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Master}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Namespace Index}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Namespace List}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}Hierarchical Index}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Class Hierarchy}{5}{section.3.1}
\contentsline {chapter}{\numberline {4}Class Index}{7}{chapter.4}
\contentsline {section}{\numberline {4.1}Class List}{7}{section.4.1}
\contentsline {chapter}{\numberline {5}File Index}{9}{chapter.5}
\contentsline {section}{\numberline {5.1}File List}{9}{section.5.1}
\contentsline {chapter}{\numberline {6}Namespace Documentation}{11}{chapter.6}
\contentsline {section}{\numberline {6.1}Ui Namespace Reference}{11}{section.6.1}
\contentsline {chapter}{\numberline {7}Class Documentation}{13}{chapter.7}
\contentsline {section}{\numberline {7.1}Code\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Page Class Reference}{13}{section.7.1}
\contentsline {subsection}{\numberline {7.1.1}Detailed Description}{14}{subsection.7.1.1}
\contentsline {subsection}{\numberline {7.1.2}Constructor \& Destructor Documentation}{14}{subsection.7.1.2}
\contentsline {subsubsection}{\numberline {7.1.2.1}$\sim $\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Code\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Page()}{14}{subsubsection.7.1.2.1}
\contentsline {subsubsection}{\numberline {7.1.2.2}Code\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Page()}{15}{subsubsection.7.1.2.2}
\contentsline {subsection}{\numberline {7.1.3}Member Function Documentation}{15}{subsection.7.1.3}
\contentsline {subsubsection}{\numberline {7.1.3.1}get()}{15}{subsubsection.7.1.3.1}
\contentsline {subsubsection}{\numberline {7.1.3.2}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Char(quint8 idx)}{16}{subsubsection.7.1.3.2}
\contentsline {subsubsection}{\numberline {7.1.3.3}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Code(\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Q\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Char chr)}{16}{subsubsection.7.1.3.3}
\contentsline {subsection}{\numberline {7.1.4}Member Data Documentation}{16}{subsection.7.1.4}
\contentsline {subsubsection}{\numberline {7.1.4.1}chars}{16}{subsubsection.7.1.4.1}
\contentsline {subsubsection}{\numberline {7.1.4.2}codes}{16}{subsubsection.7.1.4.2}
\contentsline {subsubsection}{\numberline {7.1.4.3}instance}{16}{subsubsection.7.1.4.3}
\contentsline {subsubsection}{\numberline {7.1.4.4}shift}{16}{subsubsection.7.1.4.4}
\contentsline {subsubsection}{\numberline {7.1.4.5}size}{16}{subsubsection.7.1.4.5}
\contentsline {subsubsection}{\numberline {7.1.4.6}stop}{17}{subsubsection.7.1.4.6}
\contentsline {subsubsection}{\numberline {7.1.4.7}ushift}{17}{subsubsection.7.1.4.7}
\contentsline {subsubsection}{\numberline {7.1.4.8}ustop}{17}{subsubsection.7.1.4.8}
\contentsline {section}{\numberline {7.2}Coding\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Engine Class Reference}{17}{section.7.2}
\contentsline {subsection}{\numberline {7.2.1}Detailed Description}{19}{subsection.7.2.1}
\contentsline {subsection}{\numberline {7.2.2}Constructor \& Destructor Documentation}{19}{subsection.7.2.2}
\contentsline {subsubsection}{\numberline {7.2.2.1}Coding\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Engine()}{19}{subsubsection.7.2.2.1}
\contentsline {subsubsection}{\numberline {7.2.2.2}Coding\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Engine(\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Q\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Image $\ast $img)}{19}{subsubsection.7.2.2.2}
\contentsline {subsubsection}{\numberline {7.2.2.3}$\sim $\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Coding\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Engine()}{19}{subsubsection.7.2.2.3}
\contentsline {subsection}{\numberline {7.2.3}Member Function Documentation}{20}{subsection.7.2.3}
\contentsline {subsubsection}{\numberline {7.2.3.1}decode()}{20}{subsubsection.7.2.3.1}
\contentsline {subsubsection}{\numberline {7.2.3.2}encode(\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Q\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}String \&txt)}{20}{subsubsection.7.2.3.2}
\contentsline {subsubsection}{\numberline {7.2.3.3}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Byte(\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Q\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Color pix)}{21}{subsubsection.7.2.3.3}
\contentsline {subsubsection}{\numberline {7.2.3.4}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Image()}{22}{subsubsection.7.2.3.4}
\contentsline {subsubsection}{\numberline {7.2.3.5}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Byte(\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Q\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Color pix, quint8 byte)}{22}{subsubsection.7.2.3.5}
\contentsline {subsubsection}{\numberline {7.2.3.6}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Image(\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Q\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Image $\ast $img)}{22}{subsubsection.7.2.3.6}
\contentsline {subsection}{\numberline {7.2.4}Member Data Documentation}{23}{subsection.7.2.4}
\contentsline {subsubsection}{\numberline {7.2.4.1}bitmask}{23}{subsubsection.7.2.4.1}
\contentsline {subsubsection}{\numberline {7.2.4.2}cp}{23}{subsubsection.7.2.4.2}
\contentsline {subsubsection}{\numberline {7.2.4.3}img}{23}{subsubsection.7.2.4.3}
\contentsline {section}{\numberline {7.3}imageio Class Reference}{24}{section.7.3}
\contentsline {subsection}{\numberline {7.3.1}Detailed Description}{24}{subsection.7.3.1}
\contentsline {subsection}{\numberline {7.3.2}Constructor \& Destructor Documentation}{24}{subsection.7.3.2}
\contentsline {subsubsection}{\numberline {7.3.2.1}imageio()}{24}{subsubsection.7.3.2.1}
\contentsline {subsubsection}{\numberline {7.3.2.2}$\sim $imageio()}{25}{subsubsection.7.3.2.2}
\contentsline {subsection}{\numberline {7.3.3}Member Function Documentation}{25}{subsection.7.3.3}
\contentsline {subsubsection}{\numberline {7.3.3.1}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}dstimage()}{25}{subsubsection.7.3.3.1}
\contentsline {subsubsection}{\numberline {7.3.3.2}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}srcimage()}{25}{subsubsection.7.3.3.2}
\contentsline {subsubsection}{\numberline {7.3.3.3}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}srcimage\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}size()}{26}{subsubsection.7.3.3.3}
\contentsline {subsubsection}{\numberline {7.3.3.4}load\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}image(\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Q\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}String filepath)}{26}{subsubsection.7.3.3.4}
\contentsline {subsubsection}{\numberline {7.3.3.5}save\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}image(\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Q\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}String filepath, Q\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Image img)}{26}{subsubsection.7.3.3.5}
\contentsline {subsubsection}{\numberline {7.3.3.6}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}dst\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}image(\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Q\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Image $\ast $img)}{27}{subsubsection.7.3.3.6}
\contentsline {subsection}{\numberline {7.3.4}Member Data Documentation}{27}{subsection.7.3.4}
\contentsline {subsubsection}{\numberline {7.3.4.1}dst\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}image}{27}{subsubsection.7.3.4.1}
\contentsline {subsubsection}{\numberline {7.3.4.2}src\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}image}{27}{subsubsection.7.3.4.2}
\contentsline {section}{\numberline {7.4}stegogui Class Reference}{27}{section.7.4}
\contentsline {subsection}{\numberline {7.4.1}Detailed Description}{30}{subsection.7.4.1}
\contentsline {subsection}{\numberline {7.4.2}Constructor \& Destructor Documentation}{30}{subsection.7.4.2}
\contentsline {subsubsection}{\numberline {7.4.2.1}stegogui(\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Q\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Widget $\ast $parent=0)}{30}{subsubsection.7.4.2.1}
\contentsline {subsubsection}{\numberline {7.4.2.2}$\sim $stegogui()}{31}{subsubsection.7.4.2.2}
\contentsline {subsection}{\numberline {7.4.3}Member Function Documentation}{31}{subsection.7.4.3}
\contentsline {subsubsection}{\numberline {7.4.3.1}display\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}error(\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Q\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}String errmsg)}{31}{subsubsection.7.4.3.1}
\contentsline {subsubsection}{\numberline {7.4.3.2}myembed}{31}{subsubsection.7.4.3.2}
\contentsline {subsubsection}{\numberline {7.4.3.3}myfileopen}{32}{subsubsection.7.4.3.3}
\contentsline {subsubsection}{\numberline {7.4.3.4}myfilesave}{33}{subsubsection.7.4.3.4}
\contentsline {subsubsection}{\numberline {7.4.3.5}mywithdraw}{33}{subsubsection.7.4.3.5}
\contentsline {subsubsection}{\numberline {7.4.3.6}update\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Gui()}{34}{subsubsection.7.4.3.6}
\contentsline {subsection}{\numberline {7.4.4}Member Data Documentation}{35}{subsection.7.4.4}
\contentsline {subsubsection}{\numberline {7.4.4.1}code}{35}{subsubsection.7.4.4.1}
\contentsline {subsubsection}{\numberline {7.4.4.2}fd}{35}{subsubsection.7.4.4.2}
\contentsline {subsubsection}{\numberline {7.4.4.3}img}{35}{subsubsection.7.4.4.3}
\contentsline {subsubsection}{\numberline {7.4.4.4}intpuform}{35}{subsubsection.7.4.4.4}
\contentsline {subsubsection}{\numberline {7.4.4.5}ui}{35}{subsubsection.7.4.4.5}
\contentsline {chapter}{\numberline {8}File Documentation}{37}{chapter.8}
\contentsline {section}{\numberline {8.1}codepage.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}cpp File Reference}{37}{section.8.1}
\contentsline {section}{\numberline {8.2}codepage.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{37}{section.8.2}
\contentsline {section}{\numberline {8.3}codingengine.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}cpp File Reference}{38}{section.8.3}
\contentsline {section}{\numberline {8.4}codingengine.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{39}{section.8.4}
\contentsline {section}{\numberline {8.5}imageio.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}cpp File Reference}{40}{section.8.5}
\contentsline {section}{\numberline {8.6}imageio.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{41}{section.8.6}
\contentsline {section}{\numberline {8.7}main.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}cpp File Reference}{41}{section.8.7}
\contentsline {subsection}{\numberline {8.7.1}Function Documentation}{42}{subsection.8.7.1}
\contentsline {subsubsection}{\numberline {8.7.1.1}main(int argc, char $\ast $argv[])}{42}{subsubsection.8.7.1.1}
\contentsline {section}{\numberline {8.8}R\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}E\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}A\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}D\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}M\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}E.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}md File Reference}{42}{section.8.8}
\contentsline {section}{\numberline {8.9}stegogui.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}cpp File Reference}{42}{section.8.9}
\contentsline {section}{\numberline {8.10}stegogui.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{43}{section.8.10}
\contentsline {chapter}{Index}{45}{section*.72}
