#ifndef IMAGEIO_H
#define IMAGEIO_H

#include <QImage>
#include <QFile>


class imageio
{
public:
    imageio();
    ~imageio();
    bool load_image(QString filepath);
    QImage* get_srcimage();
    QImage* get_dstimage();
    void set_dst_image(QImage *img);
    QSize get_srcimage_size();
    bool save_image(QString filepath, QImage img);  

private:
     QImage *src_image;
     QImage *dst_image;

};

#endif // IMAGEIO_H
