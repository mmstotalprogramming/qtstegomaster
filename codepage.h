#ifndef CODEPAGE_H
#define CODEPAGE_H
#include <QtCore>
#include <QHash>


class CodePage
{
public:
    static CodePage* get();
    const QChar& getChar(quint8 idx);
    quint8 getCode(QChar chr);
    virtual ~CodePage();
    const QChar stop;
    const QChar shift;
    const quint8 ustop;
    const quint8 ushift;

private:
    static CodePage* instance;
    CodePage();
    const quint8 size = 126;
    QHash<QChar, quint8> codes;
    const QChar chars[126];
};

#endif // CODEPAGE_H
