#include <QApplication>
#include "stegogui.h"
#include <QDebug>


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    stegogui gui;
    gui.show();

    return app.exec();
}
