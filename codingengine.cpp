#include "codingengine.h"
#include "codepage.h"


/**
 * Construtor create our CodePage Object
 *
 */
CodingEngine::CodingEngine() : cp(CodePage::get())
{
}


/**
 * Overloaded Construtor for setting the Source Image and CodePage Object
 * when the Engine Object is created
 *
 */

CodingEngine::CodingEngine(QImage *img) : img(img), cp(CodePage::get())
{
}


/**
 * Destroy all Heap Objects (don't won't to get Memory Leaks)
 *
 */

CodingEngine::~CodingEngine() {
    delete img;
    delete cp;
}



/**
 * Set the Source Image File
 *
 * @param QImage Pointer of the Source Image
 */

void CodingEngine::setImage(QImage *img) {
    this->img = img;
}

/**
 * Supply the new Encoded Image (Destination Image)
 *
 * @return QImage Pointer to the Destination Image
 */

QImage* CodingEngine::getImage() {
    return this->img;
}

/**
 * Set Byte Value for the Embedded Character
 *
 * @param QColor for a Pixel to be manipulated
 * @param quint8 for the Byte Value of the Character to be Set
 *
 * @return QColor the manipulated Pixel
 */

QColor CodingEngine::setByte(QColor pix, quint8 byte) {
    quint8 red = pix.red();
    quint8 blue = pix.blue();
    quint8 green = pix.green();

    red = red & ~bitmask;
    blue = blue & ~bitmask;
    green = green & ~bitmask;

    blue |= byte & bitmask;
    red |= ((byte & 0b00110000) >> 4);
    green |= ((byte & 0b00001100) >> 2);

    pix.setRgb(red, green, blue);
    return pix;
}

/**
 * Read the manipulated Pixel to get the embedded Byte Value for a Character
 *
 * @param QColor for a Pixel
 *
 * @return quint8 Byte Value for a Character
 */

quint8 CodingEngine::getByte(QColor pix) {
    quint8 red = (pix.red() & bitmask) << 4;
    quint8 blue = pix.blue() & bitmask;
    quint8 green = (pix.green() & bitmask) << 2;
    return (red | blue | green);
}

/**
 * Run through the Image and extract the hidden Characters from the Image
 * In the first Pixel is the Frequenz save at which Positions the Characters are embedded
 *
 * @return QString with the hidden Text
 */

QString CodingEngine::decode() {
    quint8 freq = getByte(img->pixelColor(0, 0));
    bool page = false;
    bool run = true;
    QString msg = "";
    uint poscount = freq;
    int width = img->width();
    int height = img->height();


    while(run && (poscount < (width * height))) {
        quint8 chrbyte = getByte(img->pixelColor(poscount % width, poscount / width));

        if(chrbyte == cp->ustop) {
            run = false;
        } else {
            if (chrbyte == cp->ushift)
                page = page ? false: true;
            else {
                if (page)
                    chrbyte += 64;

                msg.append(cp->getChar(chrbyte));
            }
        }

        poscount += freq;
    }

    return msg;
}


/**
 * Run through the Image and embedd the Characters in the Image
 * First calculate the Frequenz and embedd it in the first Pixel
 *
 */

void CodingEngine::encode(QString &txt) {
    uint len = txt.length();
    quint64 tmpfreq = img->width() * img->height() / (len * 2 + 2);
    quint8 freq = tmpfreq > 63 ? 63: tmpfreq;
    freq = freq < 1 ? 1: freq;
    uint poscount = freq;
    int width = img->width();
    bool page = false;
    img->setPixelColor(0, 0, setByte(img->pixelColor(0, 0), freq));
     uint x, y;

     for(uint i = 0; i < len; i++) {
         quint8 code = cp->getCode(txt[i]);
         x = poscount % width;
         y = poscount / width;

         if(code > 63 && !page) {
             img->setPixelColor(x, y, setByte(img->pixelColor(x, y), cp->ushift));
             poscount += freq;
             x = poscount % width;
             y = poscount / width;
             page = true;
         }

         if(code <64 && page) {
             img->setPixelColor(x, y, setByte(img->pixelColor(x, y), cp->ushift));
             poscount += freq;
             x = poscount % width;
             y = poscount / width;
             page = false;
         }

         if(page)
             img->setPixelColor(x, y, setByte(img->pixelColor(x, y), code - 64));
         else
             img->setPixelColor(x, y, setByte(img->pixelColor(x, y), code));

         poscount += freq;
     }

     x = poscount % width;
     y = poscount / width;
     img->setPixelColor(x, y, setByte(img->pixelColor(x, y), cp->ustop));
}
