#include "imageio.h"
#include <QDebug>


/**
 * Create our used Image Objects
 */

imageio::imageio()
{
    src_image = new QImage();
    dst_image = new QImage();

}


/**
 * Destroy all used Heap Objects (to avoid Memory Leaks)
 */

imageio::~imageio()
{
 delete src_image;
 delete dst_image;
}


/**
 * Load Image from the Filesystem with the given Path
 *
 * @param Filesystem Path
 * @return true if the Image was loaded / fales if the Image could not be loaded
 */

bool imageio::load_image(QString filepath)
{

    if(!src_image->load(filepath)){        
        qDebug() << "image load failed";
        return false;
    }

    return true;
}


/**
 * Returns the Source Image
 *
 * @return QImage Pointer from the Source Image
 */

QImage* imageio::get_srcimage()
{
    return src_image;
}

/**
 * Returns the Source Image Size
 *
 * @return QSize the Image Size from the Source Image
 */

QSize imageio::get_srcimage_size()
{
    return src_image->size();
}


/**
 * Save the created Image to the Filesystem
 *
 * @param Filesystem Path
 * @param Image Object which should be saved
 *
 * @return  true if the Image could be saved / false if the Image could not be written to the Filesystem
 */

bool imageio::save_image(QString filepath, QImage img)
{


    if(!img.save(filepath, "bmp")){
        qDebug() << "image save failed";
    }

    return true;
}

/**
 * Returns a Pointer for the Destination Image
 *
 * @return QImage Pointer
 */

QImage* imageio::get_dstimage()
{
    return dst_image;
}


/**
 * Set the Destination Image
 *
 * @param Pointer to which the Destination Image should be set
 */

void imageio::set_dst_image(QImage *img)
{

    dst_image = img;

}

