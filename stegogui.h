#ifndef STEGOGUI_H
#define STEGOGUI_H

#include <QtWidgets>
#include "imageio.h"
#include "codingengine.h"

namespace Ui {
class stegogui;
}

class stegogui : public QWidget
{
    Q_OBJECT

public:
    explicit stegogui(QWidget *parent = 0);
    ~stegogui();

private:
    Ui::stegogui *ui;
    QWidget *intpuform;
    QFileDialog *fd;
    imageio * img;
    CodingEngine *code;
    void updateGui();
    void display_error(QString errmsg);

private slots:
    void myfileopen();
    void myfilesave();
    void myembed();
    void mywithdraw();
};

#endif // STEGOGUI_H
