TEMPLATE = app

QT +=  core gui widgets

# TODO test whether reorder warning could be an error source!!
QMAKE_CXXFLAGS += -Wno-multichar -Wno-reorder
CONFIG += c++11

SOURCES += main.cpp \
	imageio.cpp \
	stegogui.cpp \
	codingengine.cpp \
	codepage.cpp

RESOURCES +=

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

FORMS += \
	stegogui.ui

HEADERS += \
	imageio.h \
	stegogui.h \
	codingengine.h \
	codepage.h
