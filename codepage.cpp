#include "codepage.h"


CodePage* CodePage::instance = NULL;


/**
 * Get an instance of this object.
 * @return Pointer to the CodePage Object
 */
CodePage* CodePage::get() {
    if (instance == NULL)
        instance = new CodePage();

    return instance;
}


/**
 * Initialise forward-map and reverse-map.
 * page 1: 62 most frequent chars in a text
 * special control chars: ↑ --> switch, ¦ --> stop
 * page 2: 62 chars less frequently used:
 * - numbers: 10 chars
 * - umlauts: 11  chars
 * - special: 41 chars
 */
CodePage::CodePage() : chars{
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            ' ', '\n', '!', '?', '.', ',', ';', ':', '-', '_',
            '^', '~',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            u'ä', u'ö', u'ü', u'ß', u'Ä', u'Ö', u'Ü',u'ø', u'Ø', u'å', u'Å',
            u'+', u'/', u'\\', u'*', u'=', u'"', u'\'', u'§', u'$', u'%', u'&', u'(', u')', u'{', u'}', u'<', u'>', u'[', u']', u'°', u'€', u'@', u'#', u'|', u'´', u'`',
            u'\t', u'‚', u'‘' , u'–', u'„', u'“', u'¢', u'£', u'©', u'®', u'™', u'µ', u'¥', u'½', u'¼'
        }, ustop(63), ushift(62), stop(chars[ustop]), shift(chars[ushift]) {
    for(quint8 i = 0; i < size; i++)
        codes.insert(chars[i], i);
}


CodePage::~CodePage() {

}


/**
 * Get the Character for a specified page index.
 * @param idx A page index.
 * @return QChar the appropriate Character.
 */
const QChar &CodePage::getChar(quint8 idx) {
        return chars[idx];
}


/**
 * Gets page code for a specified Character.
 * @param QChar chr Character
 * @return quint8 Code for the Character
 */
quint8 CodePage::getCode(QChar chr) {
    return codes[chr];
}
