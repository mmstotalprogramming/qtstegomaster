#ifndef CODINGENGINE_H
#define CODINGENGINE_H

#include <QColor>
#include <QImage>
#include <QString>
#include "codepage.h"


class CodingEngine
{
public:
    CodingEngine();
    CodingEngine(QImage * img);
    virtual ~CodingEngine();
    void setImage(QImage * img);
    QImage* getImage();
    void encode(QString& txt);
    QString decode();


private:
    static const quint8 bitmask = 0b00000011;

    quint8 getByte(QColor pix);
    QColor setByte(QColor pix, quint8 byte);
    QImage * img; 
    CodePage* cp;
};

#endif // CODINGENGINE_H
