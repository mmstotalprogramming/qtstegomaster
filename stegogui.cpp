#include "stegogui.h"
#include "ui_stegogui.h"


/**
 * Modified Constructor with the connection between the used Gui Elements and the corresponding Functions
 */

stegogui::stegogui(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::stegogui)
{
    ui->setupUi(this);    
    img = new imageio();
    fd = new QFileDialog();
    code = new CodingEngine();


    connect(ui->bt_loadimage, SIGNAL(clicked()), this, SLOT(myfileopen()));
    connect(ui->bt_saveimage, SIGNAL(clicked()), this, SLOT(myfilesave()));
    connect(ui->bt_embedtext, SIGNAL(clicked()), this, SLOT(myembed()));
    connect(ui->bt_withdrawtext, SIGNAL(clicked()), this, SLOT(mywithdraw()));

}


/**
 * Destroy all used Heap Objects (to avoid Memory Leaks)
 */


stegogui::~stegogui()
{
    delete fd;
    delete img;
    delete ui;
}

/**
 * Load the Open Dialog and get the Picture
 */

void stegogui::myfileopen()
{
    img->load_image(fd->getOpenFileName(this,tr("Open Image"), "/",tr("Image Files (*.bmp)")));    
    this->updateGui();
}


/**
 * Embedd the the Message Text from the Gui in the Picture
 */

void stegogui::myembed()
{

    if(img->get_dstimage()->isNull() && img->get_srcimage()->isNull()){
        this->display_error("No Picture loaded");
        return;
    }

    QString encodeText = ui->le_inputtext->toPlainText();
    code->setImage(img->get_srcimage());
    code->encode(encodeText);
    img->set_dst_image(code->getImage());
    this->updateGui();
}


/**
 * Get the Embedded Message from the Picture and Display the Message Text
 */

void stegogui::mywithdraw()
{
    QString text;

   if(img->get_dstimage()->isNull() && img->get_srcimage()->isNull()){
       this->display_error("No Picture loaded");
       return;
   } else if(img->get_dstimage()->isNull()){
          code->setImage(img->get_srcimage());
          img->set_dst_image(code->getImage());
   }

    code->setImage(img->get_dstimage());
    text = code->decode();
    ui->le_withdrawtext->setText(text);
    this->updateGui();
}



/**
 * Load the Save Dialog and save the new Picture to the Filesystem (fixed to BMP-Pictures)
 */

void stegogui::myfilesave()
{
    QString filepath;
    QImage savepic;

    filepath = fd->getSaveFileName(this, tr("Save Image"), "/", tr("Image Files (*.bmp)"));
    savepic = *code->getImage();
    savepic.save(filepath, "BMP");

}

/**
 * Update the Gui with Scaled Picture Previews
 */

void stegogui::updateGui()
{
    ui->lb_error->setText("");
    ui->lb_imagesrc->setPixmap(QPixmap::fromImage(img->get_srcimage()->scaled(400,400,Qt::KeepAspectRatio)));
    ui->lb_imagedst->setPixmap(QPixmap::fromImage(img->get_dstimage()->scaled(400,400,Qt::KeepAspectRatio)));

}


/**
 * Display Error Messages in the Gui
 *
 * @param QString Error Message to Display
 */

void stegogui::display_error(QString errmsg)
{
    ui->lb_error->setStyleSheet("QLabel {color : red; }");
    ui->lb_error->setText(errmsg);
}
